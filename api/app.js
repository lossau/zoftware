var express = require('express');
var app = express();
var mongoose = require('mongoose');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var config = require('./config');

var setupController = require('./controllers/setupController');
var apiController = require('./controllers/apiController');

var path = require('path');
var bodyParser = require('body-parser');
var passport = require('passport');

require('./config/passport');

app.use(passport.initialize());
app.use('/assets', express.static(__dirname + '/public'));

// allow CORS
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Cache-Control,Authorization');
    if (req.method === 'OPTIONS') {
        res.statusCode = 204;
        return res.end();
    }
    else {
        return next();
    }
});

app.set('view engine', 'ejs');

setupController(app);
apiController(app);

// error handler: 401
app.use(function(err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({
            "message": err.name + ": " + err.message
        });
    }
});

io.on('connection', function(socket){
    socket.on('dataUpdated', function(msg){
        io.emit('refreshSchedule', msg);
        io.emit('showAlert', msg);
    });
});

mongoose.Promise = global.Promise;

mongoose.connect(
    config.getDbConnectionString(),
    { useMongoClient: true }).then(function(res) {
        console.log('Connected to MongoDB database: ' + res.db.databaseName);
        var ipAddress = '';
        var port = process.env.PORT || 3000;
        require('dns').lookup(require('os').hostname(), function (err, add, fam) {
            ipAddress = add;
            http.listen(port, function(){
                console.log('HTTP Listening on: ' + ipAddress + ':' + port);
            });
        });
    }).catch(function(err) {
        console.log(err)
    });