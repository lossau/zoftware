var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var patientSchema = new Schema({
    name: String,
    birthDate: String,
    fatherName: String,
    motherName: String,
    reference: String,
    documents: [ { text: String, documentType: String } ],
    addresses: [ { text: String } ],
    contacts: [ { text: String, contactType: String } ]
});

var Patients = mongoose.model('Patients', patientSchema);

module.exports = Patients;
