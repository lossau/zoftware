var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var eventSchema = new Schema({
    type: { type: String },
    title: String,
    description: String,
    start: Date,
    end: Date,
    allDay: Boolean,
    schedule_id: {
        type: Schema.Types.ObjectId,
        ref: 'Schedules'
    },
    procedure_id: {
        type: Schema.Types.ObjectId,
        ref: 'Procedures'
    },
    patient: {
        type: Schema.Types.ObjectId,
        ref: 'Patients'
    }
});

var Events = mongoose.model('Events', eventSchema);

module.exports = Events;
