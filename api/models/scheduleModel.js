var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var scheduleSchema = new Schema({
    name: String,
    agenda: {
        color: String,
        textColor: String
    }
});

var Schedules = mongoose.model('Schedules', scheduleSchema);

module.exports = Schedules;
