var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var procedureSchema = new Schema({
    name: String
});

var Procedures = mongoose.model('Procedures', procedureSchema);

module.exports = Procedures;
