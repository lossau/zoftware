var mongoose = require('mongoose');
var Users = require('../models/users');

module.exports.profileRead = function(req, res) {

    // If no user ID exists in the JWT return a 401
    if (!req.payload._id) {
        res.status(401).json({
            "message": "UnauthorizedError: private profile"
        });
    } else {
        // Otherwise continue
        Users.findById(req.payload._id, function(err, user) {
                res.status(200).json(user);
        });
    }
};
