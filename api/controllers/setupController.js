var Schedules = require('../models/scheduleModel');
var Events = require('../models/eventModel');
var Procedures = require('../models/procedureModel');

module.exports = function(app) {

    app.get('/api/setup-dummy-data', function(req, res) {

        // seed database
        var starterSchedules = [
            {
                name: 'Jorge Picciani',
                agenda: {
                   color: '#337ab7',
                   textColor: 'black'
            }},
            {
                name: 'Raquel Dodge',
                agenda: {
                   color: '#5cb85c',
                   textColor: 'black'
            }},
            {
                name: 'Rodrigo Maia',
                agenda: {
                   color: '#f0ad4e',
                   textColor: 'black'}},
            {
                name: 'Erika Kokay',
                agenda: {
                   color: '#d9534f',
                   textColor: 'white'}},
            {
                name: 'Lindberg Farias',
                agenda: {
                   color: '#5bc0de',
                   textColor: 'black'}}];

        var starterProcedures = [
            {
                name: "Lavagem de Dinheiro"
            },
            {
                name: "Caixa 2"
            },
            {
                name: "Depósito em Offshore"
            },
            {
                name: "Pagamento de Propina"
            },
            {
                name: "Lobbying"
            }];


        Schedules.create(starterSchedules, function(err, results) {
            if (err) throw err;
            Procedures.create(starterProcedures, function(err, results) {
                res.send('ok');
            })
        });

    });

};