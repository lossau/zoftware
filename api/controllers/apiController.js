var Schedules = require('../models/scheduleModel');
var Events = require('../models/eventModel');
var Procedures = require('../models/procedureModel');
var Patients = require('../models/patientModel');

var bodyParser = require('body-parser');
var ObjectId = require('mongoose').Types.ObjectId;
var authentication = require('./authentication');
var profile = require('./profile');


var jwt = require('express-jwt');
var auth = jwt({
    secret: 'MY_SECRET',
    userProperty: 'payload'
});

module.exports = function(app) {

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    var populateScheduleEvents = function(schedule) {
        return new Promise(function(resolve, reject) {
            var scheduleObj = schedule.toObject();
            scheduleObj.events = [];
            Events.find({ schedule_id: { $in: ObjectId(scheduleObj._id) }}, function(err, events) {
                scheduleObj.agenda.events = events;
            }).then(function() {
                resolve(scheduleObj);
            });
        });
    };

    app.post('/api/register', authentication.register);

    app.post('/api/login', authentication.login);

    app.get('/api/profile', auth, profile.profileRead);


    // get all schedules with events
    app.get('/api/schedules', function(req, res) {
        console.log('--', req.method, req.route.path);
        Schedules.find({}, function(err, schedules) {
            var tasks = [];
            schedules.forEach(function(schedule) {
                tasks.push(populateScheduleEvents(schedule));
            });
            Promise.all(tasks).then(function(populated) {
                res.send(populated);
            });
        });

    });

    app.get('/api/availableSchedules', function(req, res) {
        console.log('--', req.method, req.route.path);
        Schedules.find({}, 'id name agenda.color agenda.textColor', function(err, schedules) {
            if (err) throw err;
            res.send(schedules);
        });
    });

    // get a schedule with events
    app.get('/api/schedule/:id', function(req, res) {
        if (req.params.id !== undefined && req.params.id !== 'undefined') {
            Schedules.findById({ _id: req.params.id }, function(err, schedule) {
                populateScheduleEvents(schedule).then(function(populated) {
                    res.send(populated);
                });
            });
        }
        else {
            res.status(400).send('Invalid Request');
        }
    });

    app.post('/api/schedule', function(req, res) {
        console.log('--', req.method, req.route.path);
        if (req.body._id) {
            Schedules.findByIdAndUpdate(req.body._id, {
                name: req.body.name,
                agenda: {
                    color: req.body.agenda.color,
                    textColor: req.body.agenda.textColor
                }
            }, function(err, schedule) {
                if (err) throw err;
                res.send('success');
            });
        }
        else {
            var newSchedule = Schedules({
                name: req.body.name,
                agenda: {
                    color: req.body.agenda.color,
                    textColor: req.body.agenda.textColor
                }
            });
            newSchedule.save(function(err) {
                if (err) throw err;
                res.send('success');
            });
        }
    });

    // get all events
    app.get('/api/events', function(req, res) {
        console.log('--', req.method, req.route.path);
        Events.find({}, function(err, events) {
            if (err) throw err;
            res.send(events);
        }).populate('procedure');
    });

    // get event by id
    app.get('/api/event/:eventId', function(req, res) {
        Events.findById(req.params.eventId, function (err, event) {
            if (err) throw err;
            res.send(event);
        }).populate('patient');
    });

    // create event and add to schedule
    app.post('/api/:schedule_id/event', function(req, res) {
        console.log('--', req.method, req.route.path);
        var newEvent = Events({
            type: req.body.type,
            description: req.body.description,
            start: req.body.start,
            end: req.body.end,
            allDay: req.body.allDay
        });

        Procedures.findById(req.body.procedure_id, function(err, procedure) {
            Schedules.findById({ _id: req.params.schedule_id }, function(err, schedule) {

                newEvent.schedule_id = schedule;
                newEvent.procedure_id = procedure;
                newEvent.patient = typeof(req.body.patient) === 'string' ? null : req.body.patient;
                newEvent.title = procedure.name + ' - ' + schedule.name;
                newEvent.save(function(err) {
                    if (err) throw err;
                });
                schedule.save(function(err) {
                    if (err) throw err;
                });
                res.send('event saved');
            });
        });
    });

    // delete event
    app.delete('/api/event', function(req, res) {
        console.log('--', req.method, req.route.path);
        Events.findByIdAndRemove(req.body._id, function(err) {
            if (err) throw err;
            res.send('Success');
        });
    });

    // update event
    app.put('/api/event', function(req, res) {
        console.log('--', req.method, req.route.path);

        // TODO: atualizar somente os dados enviados na request

        if (req.body.procedure_id || req.body.schedule_id || req.body.patient) {
            Procedures.findById(req.body.procedure_id, function(err, procedure) {
                Schedules.findById(req.body.schedule_id, function(err, schedule) {

                    var dataToUpdate = {
                        type: req.body.type,
                        title: procedure.name + '\n' + schedule.name + '\n' + (req.body.patient ? req.body.patient.name : ''),
                        start: req.body.start,
                        end: req.body.end,
                        allDay: req.body.allDay,
                        description: req.body.description,
                        procedure_id: procedure,
                        schedule_id: schedule,
                        // TODO: criar novo paciente se já não existir
                        patient: req.body.patient ? req.body.patient._id : null
                    };

                    Events.findByIdAndUpdate(req.body._id, dataToUpdate, function(err) {
                        if (err) throw err;
                        res.send({ success: true });
                    });

                });
            });
        }
        else {
            var dataToUpdate = req.body;
            Events.findByIdAndUpdate(req.body._id, dataToUpdate, function(err) {
                if (err) throw err;
                res.send({ success: true });
            });
        }


    });

    // create procedure
    app.post('/api/procedure', function(req, res) {
        console.log('--', req.method, req.route.path);
        var newProcedure = Procedures({
            name: req.body.name
        });

        newProcedure.save(function(err) {
            if (err) throw err;
            res.send('success');
        });

    });

    // add procedure to event
    app.post('/api/event/:eventId/procedure', function(req, res) {
        console.log('--', req.method, req.route.path);
        Procedures.findById({ _id: req.body.procedure_id }, function(err, procedure) {
            if (err) throw err;

            Events.findById({ _id: req.params.eventId }, function(err, event) {
                event.procedure = procedure;
                event.save(function(err) {
                    if (err) throw err;
                    res.send({ success: true });
                });
            });
        })

    });

    // get all procedures
    app.get('/api/procedures', function(req, res) {
        console.log('--', req.method, req.route.path);
        Procedures.find({}, function(err, procedures) {
            if (err) throw err;
            res.send(procedures);
        });
    });

    // -------- patients

    // create patient
    app.post('/api/patient', function(req, res) {
        console.log('--', req.method, req.route.path);
        var newPatient = Patients({
            name: req.body.name,
            birthDate: req.body.birthDate,
            fatherName: req.body.fatherName,
            motherName: req.body.motherName,
            reference: req.body.reference,
            documents: req.body.documents,
            addresses: req.body.addresses,
            contacts: req.body.contacts
        });

        newPatient.save(function(err, model) {
            if (err) throw err;
            res.send({
                status: 'success',
                patientId: model._id
            });
        });

    });


    // get patient
    app.get('/api/patient/:id', function(req, res) {
        Patients.findById(req.params.id, function (err, patient) {
            if (err) throw err;
            res.send(patient);
        });
    });

    // get all patients
    app.get('/api/patients', function(req, res) {
        console.log('--', req.method, req.route.path);
        Patients.find({}, function(err, patients) {
            if (err) throw err;
            res.send(patients);
        });
    });

    function addFullTextSearch(query, paramName, searchString) {
        if (searchString) {
            var r = "";
            var sss = searchString.split(" ");
            if (sss.length <= 1) {
                // only one word
                r = sss[0];
            } else {
                // result should look like this: (?=.*comp)(?=.*abc)(?=.*300).*
                for (var s in sss) {
                    r += "(?=.*" + sss[s] + ")";
                }
                r += ".*";
            }
            // "i" for case insensitivity
            query.where(paramName).regex(new RegExp(r, 'i'));
        }
    }

    // search patients by name
    app.get('/api/search_patients', function(req, res) {
        console.log('--', req.method, req.route.path);

        var searchString = req.query.searchTerms;
        var query = Patients.find();
        addFullTextSearch(query, "name", searchString);

        Patients.find(query, function(err, docs){
            if (err) throw err;

            var response = []
            for (var i = docs.length - 1; i >= 0; i--) {
                response.push({ _id: docs[i]._id, name: docs[i].name })
            }

            res.send(response);

        });

    });

    // add contacts to patient
    app.post('/api/patient/:patientId/contact', function(req, res) {
        console.log('--', req.method, req.route.path);

        var contacts = []
        for (let c in req.body.contacts) {
            contacts.push({
                text: req.body.contacts[c].text,
                contactType: req.body.contacts[c].contactType
            });
        }

        Patients.findByIdAndUpdate(
            req.params.patientId,
            {$pushAll: {contacts: contacts}},
            {safe: true, upsert: false},
            function(err, model) {
                if (err) throw err;
                res.send({ success: true });
            }
        );

    });

    // add addresses to patient
    app.post('/api/patient/:patientId/address', function(req, res) {
        console.log('--', req.method, req.route.path);

        var addresses = []
        for (let a in req.body.addresses) {
            addresses.push({
                text: req.body.addresses[a].text
            });
        }

        Patients.findByIdAndUpdate(
            req.params.patientId,
            {$pushAll: {addresses: addresses}},
            {safe: true, upsert: false},
            function(err, model) {
                if (err) throw err;
                res.send({ success: true });
            }
        );

    });

    // add documents to patient
    app.post('/api/patient/:patientId/document', function(req, res) {
        console.log('--', req.method, req.route.path);

        var documents = []
        for (let d in req.body.documents) {
            documents.push({
                text: req.body.documents[d].text,
                documentType: req.body.documents[d].documentType
            });
        }

        Patients.findByIdAndUpdate(
            req.params.patientId,
            {$pushAll: {documents: documents}},
            {safe: true, upsert: false},
            function(err, model) {
                if (err) throw err;
                res.send({ success: true });
            }
        );

    });

    // update patient
    app.post('/api/patient/:patientId', function(req, res) {
        console.log('--', req.method, req.route.path);

        var updatedPatient = {
            name: req.body.name,
            birthDate: req.body.birthDate,
            fatherName: req.body.fatherName,
            motherName: req.body.motherName,
            reference: req.body.reference,
            documents: req.body.documents,
            addresses: req.body.addresses,
            contacts: req.body.contacts
        };

        Patients.findByIdAndUpdate(
            req.params.patientId,
            updatedPatient,
            {new: true},
            function(err, model) {
                if (err) throw err;
                res.send({
                    status: 'success',
                    patientId: model._id
                });
            }
        );

    });

    // remove patient
    app.delete('/api/patient/:patientId', function(req, res) {
        console.log('--', req.method, req.route.path);
        Patients.findByIdAndRemove(req.params.patientId, function(err) {
            if (err) throw err;
            res.send({
                status: 'success',
                message: 'patient removed',
                patientId: req.params.patientId
            });
        });
    });

};
