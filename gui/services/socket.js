app.factory('APISocket', ['API_ADDRESS', function(API_ADDRESS) {
    var socket = io.connect(API_ADDRESS);
    return socket;
}]);