app.factory('schedulesAPIService', ['$resource', 'API_ADDRESS', function($resource, API_ADDRESS) {
  return $resource(API_ADDRESS + '/api/schedules');
}]);

app.factory('availableSchedulesService', ['$resource', 'API_ADDRESS', function($resource, API_ADDRESS) {
  return $resource(API_ADDRESS + '/api/availableSchedules');
}]);
