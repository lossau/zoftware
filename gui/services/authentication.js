app.service('authentication', ['$http', '$window', 'API_ADDRESS', '$rootScope',
    function($http, $window, API_ADDRESS, $rootScope) {

        var saveToken = function(token) {
            $window.localStorage['auth-token'] = token;
        };

        var getToken = function() {
            return $window.localStorage['auth-token'];
        };

        var logout = function() {
            $window.localStorage.removeItem('auth-token');
        };

        var isLoggedIn = function() {
            var token = getToken();
            var payload;

            if (token) {
                payload = token.split('.')[1];
                payload = $window.atob(payload);
                payload = JSON.parse(payload);

                return payload.exp > Date.now() / 1000;
            } else {
                return false;
            }
        };

        var currentUser = function() {
            if (isLoggedIn()) {
                var token = getToken();
                var payload = token.split('.')[1];
                payload = $window.atob(payload);
                payload = JSON.parse(payload);
                return {
                    email: payload.email,
                    name: payload.name
                };
            }
        };

        var register = function(user) {
            return $http.post(API_ADDRESS + '/api/register', user).then(function(res) {
                saveToken(res.data.token);
            });
        };

        var login = function(user) {
            return $http.post(API_ADDRESS + '/api/login', user).then(function(res) {
                saveToken(res.data.token);
            });
        };

        return {
            saveToken: saveToken,
            getToken: getToken,
            isLoggedIn: isLoggedIn,
            currentUser: currentUser,
            register: register,
            login: login,
            logout: logout
        };

    }
]);