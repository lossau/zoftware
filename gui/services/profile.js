app.service('profileData', ['$http', 'authentication', 'API_ADDRESS', function($http, authentication, API_ADDRESS) {

    var getProfile = function () {
      return $http.get(API_ADDRESS + '/api/profile', {
        headers: {
          Authorization: 'Bearer '+ authentication.getToken()
        }
      });
    };

    return {
      getProfile : getProfile
    };

}]);