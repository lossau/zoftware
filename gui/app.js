var app = angular.module('app', ['ngRoute', 'ui.calendar', 'ngResource', 'mgcrea.ngStrap', 'ngAnimate', 'toastr']);

app.constant('API_ADDRESS', 'http://192.168.1.5:3000');

app.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    
    // allow CORS
    // $httpProvider.defaults.useXDomain = true;

    // fix for DELETE requests
    $httpProvider.defaults.headers.delete = { 'Content-Type': 'application/json;charset=utf-8' };
    $httpProvider.defaults.headers.authorization = { 'Content-Type': 'application/json;charset=utf-8' };

    // routes
    $routeProvider
    .when('/', {
        templateUrl: 'pages/home.html',
        controller: 'homeController'
    })

    .when('/login', {
        templateUrl: 'pages/login.html',
        controller: 'loginController'
    })

    .when('/register', {
        templateUrl: 'pages/register.html',
        controller: 'registerController'
    })

    .when('/profile', {
        templateUrl: 'pages/profile.html',
        controller: 'profileController'
    })

    .when('/schedule', {
        templateUrl: 'pages/schedule.html',
        controller: 'scheduleController'
    });
    
}]);

// angular-toastr config
app.config(function(toastrConfig) {
  angular.extend(toastrConfig, {
    allowHtml: true,
    closeButton: false,
    positionClass: 'toast-bottom-right',
    closeHtml: '<button>&times;</button>',
    extendedTimeOut: 1500,
    iconClasses: {
      error: 'toast-error',
      info: 'toast-info',
      success: 'toast-success',
      warning: 'toast-warning'
    },
    messageClass: 'toast-message',
    onHidden: null,
    onShown: null,
    onTap: null,
    progressBar: true,
    tapToDismiss: true,
    templates: {
      toast: 'directives/toast/toast.html',
      progressbar: 'directives/progressbar/progressbar.html'
    },
    timeOut: 5000,
    titleClass: 'toast-title',
    toastClass: 'toast'
  });
});

// redirect unlogged user
function run($rootScope, $location, authentication) {
    $rootScope.$on('$routeChangeStart', function(event, nextRoute, currentRoute) {
        if ($location.path() === '/profile' && !authentication.isLoggedIn()) {
            $location.path('/');
        }
    });
}

app.run(['$rootScope', '$location', 'authentication', run]);



