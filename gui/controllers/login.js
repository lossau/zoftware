app.controller('loginController', ['$location', 'authentication', '$scope',
    function($location, authentication, $scope) {

        $scope.credentials = {
            email: '',
            password: ''
        };

        $scope.login = function() {
            authentication
                .login($scope.credentials)
                .catch(function(err) {
                    alert(err);
                })
                .then(function() {
                    $location.path('profile');
                });
        };
    
}]);
