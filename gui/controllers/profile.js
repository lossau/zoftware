app.controller('profileController', ['$scope', '$location', 'profileData', 'authentication', '$rootScope',
	function($scope, $location, profileData, authentication, $rootScope) {

		$rootScope.isLoggedIn = authentication.isLoggedIn();
	    $rootScope.currentUser = authentication.currentUser();

		$scope.profileData = {};

		profileData.getProfile()
			.then(function(res) {
				$scope.profileData = res.data;
			})
			.catch(function(e) {
				console.log(e);
			});

		$scope.logout = function() {
			authentication.logout();
			$location.path('/');
		};

	}
]);