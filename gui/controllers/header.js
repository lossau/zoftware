app.controller('HeaderController', ['$scope', '$location', 'authentication', '$rootScope',
    function($scope, $location, authentication, $rootScope) {

        $rootScope.isLoggedIn = authentication.isLoggedIn();
        $rootScope.currentUser = authentication.currentUser();

        $scope.isActive = function(viewLocation) {
            return viewLocation === $location.path();
        };
    }
]);