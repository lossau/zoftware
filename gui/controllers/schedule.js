app.controller('scheduleController', ['$scope', '$log', '$compile', 'uiCalendarConfig', '$window', '$resource', 'schedulesAPIService', '$http', 'API_ADDRESS', 'APISocket', '$location', '$modal', 'authentication', 'toastr',
    function($scope, $log, $compile, uiCalendarConfig, $window, $resource, schedulesAPIService, $http, API_ADDRESS, APISocket, $location, $modal, authentication, toastr) {
    
        var username = authentication.currentUser().name;

        // remove socket listeners when leaving page
        $scope.$on('$destroy', function (event) {
            APISocket.removeAllListeners();
        });

        /* alert on eventClick */
        $scope.alertOnEventClick = function( date, jsEvent, view){
            //
        };

        /* alert on Drop */
        $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){

            var eventData = {
                _id: event._id,
                title: event.title,
                start: event.start,
                end: event.end,
                allDay: event.allDay
            };

            $scope.updateEvent(eventData, function(res) {
                if (res.status === 200) {
                    APISocket.emit('dataUpdated', { data: eventData, text: 'atualizado', by: username });
                }
                else {
                    //
                }
            });

        };

        /* alert on Resize */
        $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
           var eventData = {
            _id: event._id,
            title: event.title,
            start: event.start,
            end: event.end,
            allDay: event.allDay
           };

           $scope.updateEvent(eventData, function(res) {
               if (res.status === 200) {
                   APISocket.emit('dataUpdated', { data: eventData, text: 'atualizado', by: username });
               }
               else {
                    //
               }
           });

        };

        /* add and removes an event source of choice */
        $scope.addRemoveEventSource = function(sources,source) {
          var canAdd = 0;
          angular.forEach(sources,function(value, key){
            if(sources[key] === source){
              sources.splice(key,1);
              canAdd = 1;
            }
          });
          if(canAdd === 0){
            sources.push(source);
          }
        };

        /* add custom event*/
        $scope.addEvent = function() {
          $scope.events.push({
            title: 'Open Sesame',
            start: new Date(y, m, 28),
            end: new Date(y, m, 29),
            className: ['openSesame']
          });
        };

        /* remove event */
        $scope.remove = function(index) {
          $scope.events.splice(index,1);
        };

        /* Change View */
        $scope.changeView = function(view, calendar) {
          uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
          $scope.selectedCalendarView = view;
        };

        /* Render Calendar */
        $scope.renderCalender = function(calendar) {
          if(uiCalendarConfig.calendars[calendar]){
            uiCalendarConfig.calendars[calendar].fullCalendar('render');
          }
        };

         /* Render Tooltip */
        $scope.eventRender = function(event, element, view) {
            element.attr({'tooltip': event.title,
                         'tooltip-append-to-body': true});
            $compile(element)($scope);
        };

        $scope.setLanguage = function(language) {
          if(language === 'pt-br'){
            $scope.uiConfig.calendar.dayNames = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
            $scope.uiConfig.calendar.dayNamesShort = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'];
          } else if (language === 'en-us'){
            $scope.uiConfig.calendar.dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            $scope.uiConfig.calendar.dayNamesShort = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
          }
        };

        $scope.changeSchedule = function(schedule_id_) {
            $http.get(API_ADDRESS + '/api/schedule/' + schedule_id_, {
                cache: false,
                params: {}
            }).then(function(res) {
                clearCalendar();
                if (uiCalendarConfig.calendars.proceduresSchedule) {
                    uiCalendarConfig.calendars.proceduresSchedule.fullCalendar('addEventSource', res.data.agenda);
                }
                $scope.activeSchedule = schedule_id_;
            });
        };

        $scope.addSchedule = function(schedule) {
            if ($scope.eventSources === undefined) {
                $scope.eventSources = schedule;
            } else {
                for(var i = 0; i < schedule.length; ++i) {
                  $scope.eventSources.push(schedule[i]);
                }
            }
        };

        $scope.showAllSchedules = function() {
            loadAllSchedules();
        };

        $scope.getSchedule = function(schedule_id_) {
            for (var i = 0; i < $scope.schedules.length; i++) {
                if ($scope.schedules[i]._id == schedule_id_) {
                    return [$scope.schedules[i].agenda];
                }
            }
        };

        // calendar configuration
        $scope.uiConfig = {
          calendar: {
            height: $window.innerHeight - 135,
            editable: true,
            header:{
                left: 'month,agendaWeek,agendaDay',
                center: '',
                // center: 'title',
                right: 'today prev,next'
            },
            defaultView: 'agendaWeek',
            nowIndicator: true,
            businessHours: {
                dow: [ 1, 2, 3, 4, 5 ],
                start: '8:00',
                end: '18:00',
            },
            scrollTime:'07:00:00',
            longPressDelay: 1000,
            timeFormat: {
                month: ' ',
                agenda: 'H:mm'
            },
            timezone: 'local',
            selectable: true,
            selectHelper: true,
            select: function(start, end) {

                // TODO: quando a view eh month, o end vem no dia seguinte
                // e o event fica grande

                var eventData = {
                    _type: '',
                    start: start.toDate(),
                    end: end.toDate(),
                    allDay: false,
                    title: '',
                    description: '',
                    schedule_id: $scope.activeSchedule !== 'all' ? $scope.activeSchedule : ''
                };

                $scope.openEventModal(eventData);

            },
            eventClick: function(event, jsEvent, view) {

                getEventById(event._id).then(function(res) {

                    var patient = ''
                    if (res.patient) {
                        patient = { _id: res.patient._id, name: res.patient.name };
                    }

                    var eventData = {
                        _id: res._id,
                        start: new Date(res.start),
                        end: new Date(res.end),
                        allDay: res.allDay,
                        title: res.title,
                        description: res.description,
                        type: res.type,
                        schedule_id: res.schedule_id,
                        patient: patient,
                        procedure_id: res.procedure_id
                    };

                    $scope.openEventModal(eventData);
                });


            },
            eventMouseover: function(event, jsEvent, view) {
                //
            },
            eventAfterAllRender: function(event) {
                //
            },
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender
          }
        };


        $scope.setLanguage('pt-br');
        $scope.selectedCalendarView = 'month';

        function clearCalendar() {
            if (uiCalendarConfig.calendars.proceduresSchedule != null) {
                uiCalendarConfig.calendars.proceduresSchedule.fullCalendar('removeEvents');
                uiCalendarConfig.calendars.proceduresSchedule.fullCalendar('unselect');
            }
        }

        // $scope.schedules = [];
        $scope.eventSources = [];

        var populateAvailableSchedules = function() {
            return new Promise(function(resolve, reject){
                $scope.schedules = [];
                $http.get(API_ADDRESS + '/api/availableSchedules', {
                    cache: false,
                    params: {}
                }).then(function(data) {
                    angular.forEach(data.data, function(value) {
                        $scope.schedules.push(value);
                    });
                    resolve();
                }).catch(function(err){
                    reject(err);
                });
            })
        }

        var populateAvailableProcedures = function() {
            return new Promise(function(resolve, reject){
                $scope.procedures = [];
                $http.get(API_ADDRESS + '/api/procedures', {
                    cache: false,
                    params: {}
                }).then(function(data) {
                    angular.forEach(data.data, function(value) {
                        $scope.procedures.push(value);
                    });
                    resolve();
                }).catch(function(err){
                    reject(err);
                });
            });
        };

        $scope.openEventModal = function(event) {

            $scope.newEventData = event;
            var tasks = [];
            tasks.push(populateAvailableProcedures());
            tasks.push(populateAvailableSchedules());
            Promise.all(tasks).then(function() {
                $scope.eventModal = $modal({
                    scope: $scope,
                    show: true,
                    animation: 'am-fade-and-slide-top',
                    placement: 'center',
                    templateUrl: 'pages/modal.event.html'
                });
            });

        };

        $scope.updateEvent = function(eventData) {
            $http({
                method: 'PUT',
                url: API_ADDRESS + '/api/event',
                data: eventData
            }).then(function(response) {
                if (response.status === 200) {
                    APISocket.emit('dataUpdated', { data: eventData, text: 'atualizado', by: username });
                    if ($scope.eventModal) {
                        $scope.eventModal.hide();
                    }
                }
            });
        };

        $scope.createEvent = function(eventData) {
            $http({
                method: 'POST',
                url: API_ADDRESS + '/api/' + eventData.schedule_id + '/event',
                data: eventData
            }).then(function(response) {
                if (response.status === 200) {
                    APISocket.emit('dataUpdated', { data: eventData, text: 'criado', by: username });
                    $scope.eventModal.hide();
                }
            });
        };

        $scope.deleteEvent = function(eventData) {
            $http({
                method: 'DELETE',
                url: API_ADDRESS + '/api/event',
                data: {'_id': eventData._id}
            }).then(function(response) {
                if (response.status === 200) {
                    APISocket.emit('dataUpdated', { data: eventData, text: 'removido', by: username });
                    $scope.eventModal.hide();
                }
            });
        };

        $scope.refreshSchedule = function() {
            if ($location.url() === '/schedule') {
                if ($scope.activeSchedule === 'all') {
                    loadAllSchedules();
                }
                else {
                    $scope.changeSchedule($scope.activeSchedule);
                }
            }
        };

        $scope.searchPatients = function(searchTerms) {

            if (searchTerms.length >= 1) {
                var params = { searchTerms: searchTerms };
                return $http.get(
                    API_ADDRESS + '/api/search_patients',
                    { params: params }
                )
                .then(function(res) {
                    return res.data;
                });
            }

        };

        var getEventById = function(eventId) {
            return new Promise(function(resolve, reject) {
                $http.get(API_ADDRESS + '/api/event/' + eventId, {
                    cache: false
                }).then(function(res) {
                    resolve(res.data);
                }).catch(function(err) {
                    reject(err);
                });

            });
        };

        $scope.getTypeOf = function(value) {
            return typeof value;
        };

        function loadAllSchedules() {
            var response = schedulesAPIService.query();
            response.$promise.then(function(data){
                clearCalendar();
                angular.forEach(data, function(value) {
                    if (uiCalendarConfig.calendars.proceduresSchedule) {
                        uiCalendarConfig.calendars.proceduresSchedule.fullCalendar('addEventSource', value.agenda);
                    }
                });

            });
            $scope.activeSchedule = 'all';
        }

        APISocket.on('refreshSchedule', function(msg) {
            $scope.refreshSchedule();
        });

        APISocket.on('showAlert', function(msg){
            toastr.info(
                msg.text + ' por ' + msg.by,
                'Agendamento \'' + msg.data.title.replace(/\n/g, ' - ') + '\'');
        });

        populateAvailableSchedules().then(function() {
            loadAllSchedules();
        });

}]);