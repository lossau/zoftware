app.controller('registerController', ['$location', 'authentication', '$scope',
    function($location, authentication, $scope) {

        $scope.credentials = {
            name: '',
            email: '',
            password: ''
        };

        $scope.register = function() {
            authentication
                .register($scope.credentials)
                .catch(function(err) {
                    alert(err);
                })
                .then(function() {
                    $location.path('profile');
                });
        };

}]);
