app.controller('homeController', ['$scope', '$log', 'authentication', '$rootScope',
    function($scope, $log, authentication, $rootScope) {

        $rootScope.isLoggedIn = authentication.isLoggedIn();
        $rootScope.currentUser = authentication.currentUser();

        $scope.message = '';

        if ($rootScope.isLoggedIn) {
            $scope.message = 'Bem-vindo, ' + $rootScope.currentUser.name;
        }

}]);
